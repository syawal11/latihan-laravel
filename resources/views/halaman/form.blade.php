@extends('layout.master')

@section('judul')
Halaman Form
@endsection

@section('content')
    <h1>Buat Account Baru</h1>
    
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="post">
        @csrf 
        <label>First Name</label><br><br>
        <input type="text" name="awal"><br><br>

        <label>Last Name</label><br><br>
        <input type="text" name="akhir"><br><br>

        <label>Gender</label><br><br>
        <input type="radio" name="jk" value="1">Laki-laki <br>
        <input type="radio" name="jk" value="2">Perempuan <br>
        <input type="radio" name="jk" value="3">Others<br><br>
        <label>Nationality</label><br><br>
        <select name="jk"><br><br><br><br><br>
            <option value="1">Indonesia</option>
            <option value="2">Innggris</option>
            <option value="3">Amerika</option>
        </select><br><br>
        <label>Languange Spoken</label><br><br>
        <input type="checkbox" name="jk" value="1">Bahasa Indonesia <br>
        <input type="checkbox" name="jk" value="2">English <br>
        <input type="checkbox" name="jk" value="3">Others<br><br>
        <label>Bio</label><br><br>
        <textarea name="adress" cols="30" rows="10"></textarea> <br><br>
        <input type="submit" value="Kirim">
    </form>
    
@endsection